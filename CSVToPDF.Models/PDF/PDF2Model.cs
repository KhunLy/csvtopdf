﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVToPDF.Models.PDF
{
    public class PDF2Model
    {
        public int PersonId { get; set; }

        public int Year { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string BirthDate { get; set; }

        public int Age { get; set; }

        public double HolidayHours { get; set; }

        public double StatusHours { get; set; }

        public double ConventionalHours { get; set; }

        public double TotalHours { get; set; }

        public double TotalDays { get; set; }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }
    }
}
