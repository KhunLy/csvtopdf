﻿using System.Collections.Generic;

namespace CSVToPDF.Models.PDF
{
    public class PDF1Model
    {
        public string PersonInCharge { get; set; }

        public string CostCenter { get; set; }

        public string Intitule { get; set; }

        public int Year { get; set; }

        public int Quartile { get; set; }

        public IEnumerable<PersonModel> Persons { get; set; }

        public double Total { get; set; }
    }

    public class PersonModel
    {
        public string FullName { get; set; }

        public IEnumerable<SectionModel> Sections { get; set; }

        public double TotalPerson { get; set; }
    }

    public class SectionModel
    {
        public string Section { get; set; }

        public string Description { get; set; }

        public double Period1Amount { get; set; }
        public double Period2Amount { get; set; }
        public double Period3Amount { get; set; }

        public double TotalSection { get; set; }
    }
}
