﻿using CSVToPDF.CSV.Mappers;
using CSVToPDF.CSV.Models;
using CSVToPDF.Dependencies.Services;
using CSVToPDF.Models.PDF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVToPDF.ViewModels
{
    class PDF2ControlViewModel : PDFControlViewModelBase<CSV2Line, PDF2Model>
    {
        public PDF2ControlViewModel(IPDFService<PDF2Model> pdfService) : base(pdfService)
        {
        }

        public override string Title { get => "Congés"; }

        protected override void GeneratePDFs(PDF2Model model)
        {
            PDFService.CreatePDF(model, $"{FolderName}/{model.Year} - {model.PersonId} - {model.FullName}.pdf");
        }

        protected override IEnumerable<PDF2Model> Map(IEnumerable<CSV2Line> lines)
        {
            return lines.ToPDF2Model();
        }
    }
}
