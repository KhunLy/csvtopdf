﻿using CSVToPDF.Dependencies.Services;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToolBox.CSV.Extensions;

namespace CSVToPDF.ViewModels
{
    abstract class PDFControlViewModelBase<TCSV,TPDFModel> : BindableBase
        where TCSV: new()
    {
        public abstract string Title
        {
            get;
        }

        private IEnumerable<string> _CSV;
        public IEnumerable<string> CSV
        {
            get { return _CSV; }
            set { _CSV = value; GetPDFCommand?.RaiseCanExecuteChanged(); }
        }


        private int _PdfCount;
        public int PdfCount
        {
            get { return _PdfCount; }
            set { SetProperty(ref _PdfCount, value); }
        }

        private int _Progression;
        public int Progression
        {
            get { return _Progression; }
            set { SetProperty(ref _Progression, value); }
        }

        private DelegateCommand _GetPDFCommand;
        public DelegateCommand GetPDFCommand
        {
            get { return _GetPDFCommand = _GetPDFCommand ?? new DelegateCommand(GetPDF, CanGetPDF); }
        }

        private DelegateCommand _CancelCommand;
        public DelegateCommand CancelCommand
        {
            get { return _CancelCommand = _CancelCommand ?? new DelegateCommand(Cancel, CanCancel); }
        }

        public string FolderName { get; set; }

        protected readonly IPDFService<TPDFModel> PDFService;

        public CancellationTokenSource Cts { get; set; }

        private bool _IsRunning;
        public bool IsRunning
        {
            get { return _IsRunning; }
            set { SetProperty(ref _IsRunning, value); CancelCommand?.RaiseCanExecuteChanged(); GetPDFCommand?.RaiseCanExecuteChanged(); }
        }

        private bool _IsCancelling;
        public bool IsCancelling
        {
            get { return _IsCancelling; }
            set { SetProperty(ref _IsCancelling, value); CancelCommand?.RaiseCanExecuteChanged(); }
        }

        public PDFControlViewModelBase(IPDFService<TPDFModel> pdfService)
        {
            PDFService = pdfService;
        }

        private void GetPDF()
        {
            if (string.IsNullOrWhiteSpace(FolderName))
                return;

            IEnumerable<TCSV> lines = CSV.Deserialize<TCSV>();

            IEnumerable<TPDFModel> models = Map(lines);

            PdfCount = models.Count();

            IsRunning = true;

            Cts = new CancellationTokenSource();

            Task.Run(() =>
            {
                foreach(var model in models)
                {
                    if (Cts.IsCancellationRequested)
                        break;
                    GeneratePDFs(model);
                    Progression++;
                }
                Reset();
            }, Cts.Token);
        }

        protected abstract IEnumerable<TPDFModel> Map(IEnumerable<TCSV> lines);

        protected abstract void GeneratePDFs(TPDFModel model);

        private bool CanGetPDF()
        {
            return CSV != null && !IsRunning;
        }

        private void Cancel()
        {
            Cts.Cancel();
            IsCancelling = true;
        }

        private bool CanCancel()
        {
            return IsRunning && !IsCancelling;
        }

        private void Reset()
        {
            Progression = 0;
            FolderName = null;
            CSV = null;
            Cts.Dispose();
            IsRunning = false;
            IsCancelling = false;
        }
    }
}
