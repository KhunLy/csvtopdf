﻿using Prism.Commands;
using Prism.Mvvm;
using System;

namespace CSVToPDF.ViewModels
{
    class MainWindowViewModel: BindableBase
    {
        private object _currentVM;

        public MainWindowViewModel()
        {
            NavigateCmd = new DelegateCommand<Type>(t =>
            {
                CurrentVM = CommonServiceLocator.ServiceLocator.Current.GetService(t);
            });
            CurrentVM = CommonServiceLocator.ServiceLocator.Current.GetService(typeof(PDF1ControlViewModel));
        }

        public object CurrentVM
        {
            get => _currentVM;
            private set => SetProperty(ref _currentVM, value);
        }


        public DelegateCommand<Type> NavigateCmd { get; }
    }
}
