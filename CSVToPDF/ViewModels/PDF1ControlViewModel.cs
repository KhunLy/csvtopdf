﻿using CSVToPDF.CSV.Mappers;
using CSVToPDF.CSV.Models;
using CSVToPDF.Dependencies.Services;
using CSVToPDF.Models.PDF;
using System.Collections.Generic;

namespace CSVToPDF.ViewModels
{
    class PDF1ControlViewModel : PDFControlViewModelBase<CSV1Line, PDF1Model>
    {
        public PDF1ControlViewModel(IPDFService<PDF1Model> pdfService) : base(pdfService)
        {
        }

        public override string Title { get => "Rapports Trimestriels"; }

        protected override void GeneratePDFs(PDF1Model model)
        {
            PDFService.CreatePDF(model, $"{FolderName}/T{model.Quartile} {model.Year} - {model.CostCenter} - {model.PersonInCharge}.pdf");
        }

        protected override IEnumerable<PDF1Model> Map(IEnumerable<CSV1Line> lines)
        {
            return lines.ToPDF1Model();
        }
    }
}
