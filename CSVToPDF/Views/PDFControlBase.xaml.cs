﻿using System.Windows.Controls;

namespace CSVToPDF.Views
{
    /// <summary>
    /// Logique d'interaction pour PDF1Control.xaml
    /// </summary>
    public partial class PDFControlBase : UserControl
    {
        public PDFControlBase()
        {
            InitializeComponent();
        }
    }
}
