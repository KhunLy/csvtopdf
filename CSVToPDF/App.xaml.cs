﻿using CSVToPDF.Dependencies.Services;
using CSVToPDF.Models.PDF;
using CSVToPDF.PDF.Services;
using CSVToPDF.Views;
using Microsoft.AspNetCore.Razor.Language;
using Prism.Ioc;
using Prism.Unity;
using System.Windows;

namespace CSVToPDF
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<MainWindow>();
            containerRegistry.Register<RazorEngine>();
            containerRegistry.Register<IPDFService<PDF1Model>,PDF1Service>();
            containerRegistry.Register<IPDFService<PDF2Model>,PDF2Service>();
        }
    }
}
