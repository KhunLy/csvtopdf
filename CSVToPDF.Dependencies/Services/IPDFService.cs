﻿using CSVToPDF.Models.PDF;

namespace CSVToPDF.Dependencies.Services
{
    public interface IPDFService<T>
    {
        bool CreatePDF(T model, string destination);
    }
}