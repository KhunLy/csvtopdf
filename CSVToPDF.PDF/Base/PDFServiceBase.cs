﻿using CSVToPDF.PDF.Resources;
using RazorEngineCore;
using SelectPdf;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace CSVToPDF.PDF.Base
{
    public abstract class BasePDFService<T>
    {
        private readonly RazorEngine _razor;

        protected virtual PdfPageOrientation Orientation { get => PdfPageOrientation.Portrait; }
        protected abstract float HeaderHeight { get; }
        protected virtual string Header { get => typeof(PDFResources).GetProperty(typeof(T).Name.Replace("Model", "HtmlHeader"), BindingFlags.Static | BindingFlags.NonPublic)?.GetValue(null) as string ?? string.Empty; }
        protected abstract float FooterHeight { get; }
        protected virtual string Footer { get => typeof(PDFResources).GetProperty(typeof(T).Name.Replace("Model", "HtmlFooter"), BindingFlags.Static|BindingFlags.NonPublic)?.GetValue(null) as string ?? string.Empty; }
        protected virtual string Css { get => typeof(PDFResources).GetProperty(typeof(T).Name.Replace("Model", "CssMain"), BindingFlags.Static | BindingFlags.NonPublic)?.GetValue(null) as string ?? string.Empty; }

        protected T Model { get; private set; }

        public BasePDFService(RazorEngine razor)
        {
            _razor = razor;
        }

        public virtual bool CreatePDF(T model, string destination)
        {
            try
            {
                Model = model;
                HtmlToPdf converter = new HtmlToPdf();

                // header
                converter.Options.DisplayHeader = true;
                converter.Header.Height = HeaderHeight;
                PdfHtmlSection headerHtml = new PdfHtmlSection(Header, "");
                headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
                headerHtml.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
                converter.Header.Add(headerHtml);

                //footer
                converter.Options.DisplayFooter = true;
                converter.Footer.Height = FooterHeight;
                PdfHtmlSection footerHtml = new PdfHtmlSection(Footer, "");
                footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
                footerHtml.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
                converter.Footer.Add(footerHtml);
                // footer pagination

                PdfTextSection text = CreateNumberingSection();
                converter.Footer.Add(text);

                // content
                string content = GetMainCss(Css);

                content += GetContent();

                converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
                converter.Options.PdfPageOrientation = Orientation;
                converter.Options.PdfPageSize = PdfPageSize.A4;

                PdfDocument doc = converter.ConvertHtmlString(content);
                doc.Save(destination);
                doc.Close();
                return true;
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw;
            }
        }

        protected virtual PdfTextSection CreateNumberingSection()
        {
            PdfTextSection text = new PdfTextSection(0, 80,
                        "page {page_number}/{total_pages}  ",
                        new System.Drawing.Font("Arial", 12));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;
            return text;
        }

        private string GetMainCss(string ressource)
        {
            string css = ressource;
            css = css.Replace("\n", "");
            css = css.Replace("\r", "");
            return $"<style>{css}</style>";
        }

        protected abstract string GetContent();

        protected string CreateView<TModel>(TModel model)
        {
            string razorContent = typeof(PDFResources).GetProperty(typeof(T).Name.Replace("Model", "HtmlMain"), BindingFlags.Static | BindingFlags.NonPublic)?.GetValue(null) as string ?? string.Empty;
            var template = _razor.Compile<RazorEngineTemplateBase<TModel>>(razorContent);
            return template.Run(instance =>
            {
                instance.Model = model;
            });
        }
    }
}
