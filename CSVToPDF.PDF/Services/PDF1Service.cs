﻿using CSVToPDF.Dependencies.Services;
using CSVToPDF.Models.PDF;
using CSVToPDF.PDF.Base;
using RazorEngineCore;
using SelectPdf;
using System.Drawing;
using System.Linq;

namespace CSVToPDF.PDF.Services
{
    public class PDF1Service : BasePDFService<PDF1Model>, IPDFService<PDF1Model>
    {
        public PDF1Service(RazorEngine razor) : base(razor)
        {
        }

        protected override PdfPageOrientation Orientation { get => PdfPageOrientation.Landscape; }
        protected override float HeaderHeight { get => 100; }
        protected override float FooterHeight { get => 100; }

        protected override string GetContent()
        {
            return CreateView(Model);
        }

        protected override PdfTextSection CreateNumberingSection()
        {
            PdfTextSection text = new PdfTextSection(0, 80,
                        "www unamur.be     page {page_number}/{total_pages}  ",
                        new System.Drawing.Font("Arial", 12));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;
            text.Font = new Font(text.Font.FontFamily, text.Font.Size*0.7f);
            return text;
        }
    }
}
