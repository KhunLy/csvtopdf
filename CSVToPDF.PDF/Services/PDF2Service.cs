﻿using CSVToPDF.Dependencies.Services;
using CSVToPDF.Models.PDF;
using CSVToPDF.PDF.Base;
using RazorEngineCore;
using SelectPdf;

namespace CSVToPDF.PDF.Services
{
    public class PDF2Service : BasePDFService<PDF2Model>, IPDFService<PDF2Model>
    {
        public PDF2Service(RazorEngine razor) : base(razor)
        {
        }

        protected override PdfPageOrientation Orientation { get => PdfPageOrientation.Portrait; }
        protected override float HeaderHeight { get => 100; }
        protected override float FooterHeight { get => 100; }

        protected override string Header => base.Header
            .Replace("__Model.Year__", Model.Year.ToString())
            .Replace("__Model.Year1__", (Model.Year + 1).ToString());

        protected override string GetContent()
        {
            return CreateView(Model);
        }
    }
}
