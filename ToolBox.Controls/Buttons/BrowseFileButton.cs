﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ToolBox.Controls.Buttons
{
    public class BrowseFileButton : Button
    {
        public static readonly DependencyProperty FileNameProperty = DependencyProperty.Register(nameof(FileName), typeof(string), typeof(BrowseFolderButton), null);
        
        public static readonly DependencyProperty FileContentProperty = DependencyProperty.Register(nameof(FileContent), typeof(string), typeof(BrowseFolderButton), null);
        
        public string FileName
        {
            get { return GetValue(FileNameProperty) as string; }
            set { SetValue(FileNameProperty, value); }
        }

        public string FileContent
        {
            get { return GetValue(FileContentProperty) as string; }
            set { SetValue(FileContentProperty, value); }
        }

        public BrowseFileButton()
        {
            Content = "Browse";
            Click += Browse;
        }

        private void Browse(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                FileName = openFileDialog.FileName;
                FileContent = File.ReadAllText(openFileDialog.FileName);
            }
        }
    }
}
