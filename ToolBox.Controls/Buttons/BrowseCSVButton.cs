﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ToolBox.Controls.Buttons
{
    public class BrowseCSVButton : Button
    {
        public static readonly DependencyProperty FileNameProperty = DependencyProperty.Register(nameof(FileName), typeof(string), typeof(BrowseFolderButton), null);

        public static readonly DependencyProperty CSVProperty = DependencyProperty.Register(nameof(CSV), typeof(IEnumerable<string>), typeof(BrowseCSVButton), null);
        
        public IEnumerable<string> CSV
        {
            get { return GetValue(CSVProperty) as IEnumerable<string>; }
            set { SetValue(CSVProperty, value); }
        }

        public string FileName
        {
            get { return GetValue(FileNameProperty) as string; }
            set { SetValue(FileNameProperty, value); }
        }

        public BrowseCSVButton()
        {
            Content = "Browse";
            Click += Browse;
        }

        private void Browse(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.DefaultExt = ".csv";
            dlg.Filter = "csv (.csv)|*.csv";

            if (dlg.ShowDialog() == true)
            {
                CSV = ReadLines(dlg.FileName);
                FileName = dlg.FileName;
            }
        }

        private IEnumerable<string> ReadLines(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    yield return reader.ReadLine();
                }
            }
        }
    }
}
