﻿using System.Windows;
using C = System.Windows.Controls;
using System.Windows.Forms;

namespace ToolBox.Controls.Buttons
{
    public class BrowseFolderButton : C.Button
    {
        public static readonly DependencyProperty FolderNameProperty = DependencyProperty.Register(nameof(FolderName), typeof(string), typeof(BrowseFolderButton), null);
        
        public string FolderName
        {
            get { return GetValue(FolderNameProperty) as string; }
            set { SetValue(FolderNameProperty, value); }
        }

        public BrowseFolderButton()
        {
            Content = "Browse";
            Click += Browse;
        }

        private void Browse(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    FolderName = fbd.SelectedPath;
                }
            }
        }
    }
}
