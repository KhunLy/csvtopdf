﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ToolBox.CSV.Attributes;

namespace ToolBox.CSV.Extensions
{
    public static class CSVConverter
    {
        public static IEnumerable<T> Deserialize<T>(this IEnumerable<string> csv, char separator = ',', int skipedRows = 1)
            where T : new()
        {
            string[] headers = csv.FirstOrDefault()?.Split(separator);
            if (headers == null) throw new ArgumentException();
            foreach (string[] line in csv.Skip(skipedRows).Select(l => l.Split(separator)))
            {
                T item = new T();
                if (line.Length != headers.Length) throw new ArgumentException();
                for (int i = 0; i < line.Length; i++)
                {
                    PropertyInfo prop = typeof(T).GetProperties()
                        .FirstOrDefault(p => p.GetCustomAttribute<CSVNameAttribute>()?.Name == headers[i] || p.Name == headers[i]);
                    if (prop == null) throw new ArgumentException();
                    TypeConverter converter =
                        TypeDescriptor.GetConverter(prop.PropertyType);
                    object value = converter.ConvertFromString(null, CultureInfo.InvariantCulture, line[i]);
                    prop.SetValue(item, value);
                }
                yield return item;
            }
        }
    }
}
