﻿using System;

namespace ToolBox.CSV.Attributes
{
    public class CSVNameAttribute : Attribute
    {
        public string Name { get; set; }
        public CSVNameAttribute(string name)
        {
            Name = name;
        }
    }
}
