﻿using ToolBox.CSV.Attributes;

namespace CSVToPDF.CSV.Models
{
    public partial class CSV1Line
    {
        [CSVName("Responsable")]
        public string PersonInCharge { get; set; }

        [CSVName("Centre de Coût")]
        public string CostCenter { get; set; }

        [CSVName("Nom Complet")]
        public string FullName { get; set; }

        [CSVName("Rubrique")]
        public string Section { get; set; }

        [CSVName("Txte descriptif")]
        public string Description { get; set; }

        [CSVName("Montant")]
        public double Amount { get; set; }

        public int Year { get; set; }

        public string Month { get; set; }
        [CSVName("Intitulé")]
        public string Intitule { get; set; }
    }
}
