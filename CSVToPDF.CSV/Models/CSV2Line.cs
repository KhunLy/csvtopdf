﻿using ToolBox.CSV.Attributes;

namespace CSVToPDF.CSV.Models
{
    public class CSV2Line
    {
        [CSVName("ID personne externe")]
        public int PersonId { get; set; }

        [CSVName("Année")]
        public int Year { get; set; }

        [CSVName("Nom")]
        public string LastName { get; set; }

        [CSVName("Prénom")]
        public string FirstName { get; set; }

        [CSVName("Naissance")]
        public string BirthDate { get; set; }

        [CSVName("Age")]
        public int Age { get; set; }

        [CSVName("Heures Vacances Légales Mois")]
        public double HolidayHours { get; set; }

        [CSVName("Heures Congés Statut Mois")]
        public double StatusHours { get; set; }

        [CSVName("Heures_Congés_Conventionnelles")]
        public double ConventionalHours { get; set; }

        [CSVName("Heures Totales")]
        public double TotalHours { get; set; }

        [CSVName("Jours Totaux")]
        public double TotalDays { get; set; }
    }
}
