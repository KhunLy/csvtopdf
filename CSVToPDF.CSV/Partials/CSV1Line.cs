﻿namespace CSVToPDF.CSV.Models
{
    public partial class CSV1Line
    {
        public int MonthInt
        {
            get
            {
                switch (Month.ToLower())
                {
                    case "janvier":
                        return 1;
                    case "février":
                        return 2;
                    case "mars":
                        return 3;
                    case "avril":
                        return 4;
                    case "mai":
                        return 5;
                    case "juin":
                        return 6;
                    case "juillet":
                        return 7;
                    case "août":
                        return 8;
                    case "septembre":
                        return 9;
                    case "octobre":
                        return 10;
                    case "novembre":
                        return 11;
                    case "décembre":
                        return 12;
                    default:
                        return 0;

                }
            }
        }

        public int Quartile
        {
            get
            {
                return (MonthInt - 1) / 3 + 1;
            }
        }
    }
}
