﻿using CSVToPDF.CSV.Models;
using CSVToPDF.Models.PDF;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CSVToPDF.CSV.Mappers
{
    public static class CSVToModelExtensions
    {
        public static IEnumerable<PDF1Model> ToPDF1Model(this IEnumerable<CSV1Line> csv)
        {
            return csv.GroupBy(x => (x.CostCenter, x.PersonInCharge, x.Year, x.Quartile, x.Intitule))
                .Select(g => new PDF1Model
                {
                    CostCenter = g.Key.CostCenter,
                    PersonInCharge = g.Key.PersonInCharge,
                    Intitule = g.Key.Intitule,
                    Year = g.Key.Year,
                    Quartile = g.Key.Quartile,
                    Total = g.Sum(x => x.Amount),
                    Persons = g.GroupBy(x => x.FullName)
                        .Select(p => new PersonModel
                        {
                            FullName = p.Key,
                            TotalPerson = p.Sum(x => x.Amount),
                            Sections = p.GroupBy(x => x.Description)
                                .Select(s => new SectionModel
                                {
                                    Description = s.Key,
                                    Period1Amount = s.Where(x => x.MonthInt % 3 == 1).Sum(x => x.Amount),
                                    Period2Amount = s.Where(x => x.MonthInt % 3 == 2).Sum(x => x.Amount),
                                    Period3Amount = s.Where(x => x.MonthInt % 3 == 0).Sum(x => x.Amount),
                                    TotalSection = s.Sum(x => x.Amount)
                                })
                        })
                });
        }

        public static IEnumerable<PDF2Model> ToPDF2Model(this IEnumerable<CSV2Line> csv)
        {
            return csv.Select(l => new PDF2Model
            {
                PersonId = l.PersonId,
                Age = l.Age,
                // convert to DateTime ?
                BirthDate = l.BirthDate,
                ConventionalHours = l.ConventionalHours,
                FirstName = l .FirstName,
                HolidayHours = l.HolidayHours,
                LastName = l.LastName,
                StatusHours = l.StatusHours,
                TotalDays = l.TotalDays,
                TotalHours = l.TotalHours,
                Year = l.Year
            });
        }
    }
}
